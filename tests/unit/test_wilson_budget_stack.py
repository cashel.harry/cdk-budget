import aws_cdk as core
import aws_cdk.assertions as assertions

from wilson_budget.wilson_budget_stack import WilsonBudgetStack

# example tests. To run these tests, uncomment this file along with the example
# resource in wilson_budget/wilson_budget_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = WilsonBudgetStack(app, "wilson-budget")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
