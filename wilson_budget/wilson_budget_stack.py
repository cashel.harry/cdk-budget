# Import the necessary modules
from aws_cdk import Stack
from constructs import Construct
from aws_cdk import aws_budgets as budgets
from aws_cdk import aws_s3 as s3

# Constants

SERVICES = [
    ("LambdaBudget", "AWS Lambda", 50),
    ("S3Budget", "Amazon Simple Storage Service", 1200),
    ("RDSBudget", "Amazon Relational Database Service", 8000),
    ("EC2Budget", "Amazon Elastic Compute Cloud - Compute", 5000),
    ("EC2-OtherBudget", "Amazon Elastic Compute Cloud - Other", 2000),
    ("EC2-ELBBudget", "Amazon Elastic Compute Cloud - Elastic Load Balancing", 500),
    ("CloudWatchBudget", "Amazon CloudWatch", 500),
]
        
class WilsonBudgetStack(Stack):
    def __init__(self, scope, id, **kwargs):
        super().__init__(scope, id, **kwargs)

        total_budget = budgets.CfnBudget(
            self, "TotalBudget",
            budget=budgets.CfnBudget.BudgetDataProperty(
                budget_type="COST",
                time_unit="MONTHLY",
                budget_limit=budgets.CfnBudget.SpendProperty(
                    amount=25000,
                    unit="USD"
                ),
                budget_name="TotalBudget",
            ),

            notifications_with_subscribers=[budgets.CfnBudget.NotificationWithSubscribersProperty(
                    notification=budgets.CfnBudget.NotificationProperty(
                        comparison_operator="GREATER_THAN",
                        threshold=80,
                        threshold_type="PERCENTAGE",
                        notification_type="ACTUAL"
                    ),
                    subscribers=[budgets.CfnBudget.SubscriberProperty(
                        address="test@test.com",
                        subscription_type="EMAIL"
                    )]
                )]
        )

        for service in SERVICES:
            budget = budgets.CfnBudget(
                self, service[0],
                budget=budgets.CfnBudget.BudgetDataProperty(
                    budget_type="COST",
                    time_unit="MONTHLY",
                    budget_limit=budgets.CfnBudget.SpendProperty(
                        amount=service[2],
                        unit="USD"
                    ),
                    budget_name=service[0],
                    cost_filters={
                        "Service": [service[1]]
                    }
                ),
                notifications_with_subscribers=[budgets.CfnBudget.NotificationWithSubscribersProperty(
                    notification=budgets.CfnBudget.NotificationProperty(
                        comparison_operator="GREATER_THAN",
                        threshold=80,
                        threshold_type="PERCENTAGE",
                        notification_type="ACTUAL"
                    ),
                    subscribers=[budgets.CfnBudget.SubscriberProperty(
                        address="test@test.com",
                        subscription_type="EMAIL"
                    )]
                )]
            )
